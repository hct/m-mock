easy middleware

### install
```bash
npm install m-mock
```

### usage
```js
const path = require('path')
const express = require('express')
const mmock = require('m-mock')

const app = express()
app.use('/backend', mmock(path.join(__dirname, 'mock')))
```

### support
**JavaScript Handler**

you can write a JavaScript hanlder to process specify request

```js
/**
 * session.js
 */
module.exports = (req, res) => {
  res.cookie('sessionID', "asdas23e21-asdasd23432-123absdgasd6t1")
  res.send({
    code: 200,
    data: {
      uid: 123
    }
  })
}
```
**JSON File**

```json
{
  "code": 200,
  "data": {
    "name": "hangzhou"
  }
}
```

**Template File**

`.hanlders` file, use [art-template](https://github.com/aui/art-template) syntax
```handlebars
{{set page = page || 1}}
{{set size = size || 10}}
{
  "code": 200,
  "data": {
    "pagination": {
      "page": {{page}},
      "size": {{size}},
      "total": 500,
      "totalPage": {{500 / size}}
    },
    "result": [
      {{each helpers.range(0, 5)}}
        {
          "id": {{page * size + $index}}
        }
      {{/each}}
    ]
  }
}

```
