const fs = require('fs')
const path = require('path')
const artTemplate = require('art-template')

const _JsonHanlder = (file) => {
  let json
  try {
    json = JSON.parse(fs.readFileSync(file, 'utf-8'))
  } catch (e) {
    console.error(e)
    json = {}
  }
  return (req, res) => {
    res.append('Content-type', 'application/json')
    res.send(json)
  }
}

const TplHelpers = {
  range (from, to) {
    return Array.from({length: to - from + 1}, (item, index) => from + index)
  },
  randomNumber (from, to) {
    return from + Math.random() * (to - from + 1) >> 0
  },
  randomArray (arr) {
    return arr(TplHelpers.randomNumber(0, arr.length - 1))
  }
}

const _HandlebarsHandler = (file) => {
  let content = fs.readFileSync(file, 'utf-8')
  let template = artTemplate.compile(content)
  return (req, res) => {
    res.append('Content-type', 'application/json')
    let data = Object.assign({}, global, req.params, req.query, req.body, {
      helpers: TplHelpers
    })
    res.send(template(data))
  }
}

const _JsHanlder = (file) => {
  let func = require(file)
  return (req, res, next) => {
    let data = Object.assign({}, global, req.params, req.query, req.body)
    req.data = data
    res.append('Content-type', 'application/json')
    return func(req, res, next)
  }
}

let extHanlderMap = {
  '.js': _JsHanlder,
  '.handlebars': _HandlebarsHandler,
  '.json': _JsonHanlder
}

const Handler = (file) => {
  let ext = path.extname(file)
  let generator = extHanlderMap[ext]
  if (generator) {
    return generator(file)
  } else {
    return () => {}
  }
}

module.exports = Handler
