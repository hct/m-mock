const path = require('path')
const chokidar = require('chokidar')
const pathToRegexp = require('path-to-regexp')

const utils = require('./utils')
const hanlder = require('./hanlder')

const DYNAMIC_CHAR = '_'

/**
 * 静态路径优于动态路径
 * @param {string} f1 
 * @param {string} f2 
 */
const pathComparator = (f1, f2) => {
  let length = Math.min(f1.length, f2.length)
  for (let i = 0; i < length; i++) {
    if (f1[i] === DYNAMIC_CHAR && f2[i] !== DYNAMIC_CHAR) {
      return 1
    } else if (f2[i] === DYNAMIC_CHAR && f1[i] !== DYNAMIC_CHAR) {
      return -1
    }
  }
  return 0
}

/**
 * @param {Array<string>} files 
 * @return {{[method: string]: Array<string>}}
 */
const groupByMethod = (files, base) => {
  let result = {}
  files.forEach(file => {
    let method = file.replace(base, '').replace(/^\/?/, '').match(/(\w*)/)[1].toLocaleLowerCase()

    result[method] = result[method] || []
    result[method].push(file)
  })
  return result
}

const toUrlPath = (file, base) => {
  let ext = path.extname(file)
  return file.replace(ext, '').replace(base, '').replace('\\', '/').replace(/^\/\w+/, '').replace(`/${DYNAMIC_CHAR}`, '/:')
}

/**
 * @param {string} file 
 * @param {string} base
 * @return {{regex: Regexp, keys: {name: string}[], hanlder: Function}}
 */
const toUrl = (file, base) => {
  let path = toUrlPath(file, base)
  console.log(path)
  let keys = []
  let regex = pathToRegexp(path, keys)

  return {
    regex,
    keys,
    hanlder: hanlder(file)
  }
}

/**
 * @param {string} folder mock文件目录，必须绝对路径
 */
const refreshUlrs = (folder) => {
  try {
    if (!folder.startsWith('/')) {
      throw new Error('❌: Folder must be an absolute path')
    }
    let base = folder

    let files = groupByMethod(utils.getAllFiles(folder), base)
    let urls = {}

    for (let method in files) {
      urls[method] = files[method].sort(pathComparator).map(file => toUrl(file, base))
    }
    return urls
  } catch (e) {
    console.error(e)
    return urls
  }
}

let urls
module.exports.scan = (folder) => {
  urls = refreshUlrs(folder)
  let watcher = chokidar.watch(folder)
  watcher.on('change', () => {
    console.log('change')
    urls = refreshUlrs(folder)
  })
}

module.exports.getUrls = () => {
  return urls
}
