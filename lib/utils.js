const fs = require('fs')
const path = require('path')

/**
 * 遍历文件 
 * @param {string} file 
 * @param {Function} callback 
 */
const traverse = (file, callback) => {
  let stat = fs.statSync(file)
  if (stat.isFile()) {
    callback(file)
  } else if (stat.isDirectory()) {
    fs.readdirSync(file).forEach(f => {
      traverse(path.join(file, f), callback)
    })
  }
}
/**
 * @param {string} folder mock文件目录 
 * @return {Array<string>} 文件列表
 */
const getAllFiles = (folder) => {
  let result = []
  traverse(folder, (file) => {
    result.push(file)
  })
  return result
}

module.exports.getAllFiles = getAllFiles
