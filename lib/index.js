const parser = require('./parser')

module.exports = (folder) => {
  parser.scan(folder)
  const middleware = (req, res, next) => {
    let urls = parser.getUrls()[req.method.toLowerCase()]
    let url = urls.find(url => url.regex.test(req.path))
    if (!url) {
      return next()
    }
    req.params = req.params || {}

    let keys = url.keys
    let match = url.regex.exec(req.path)
    match.slice(1).forEach((str, i) => {
      req.params[keys[i].name] = str
    })

    url.hanlder(req, res)
  }
  return middleware
}
