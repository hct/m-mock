const path = require('path')
const express = require('express')
const mockya = require('../lib/index')

const app = express()

app.use('/xhr', mockya(path.join(__dirname, 'mock')))

app.use('//test', (req, res) => {
  res.send('this is response')
})

app.listen(8100, (err) => {
  if (err) {
    console.error(err)
    return
  }
  console.log('mock server listen on 8100!\n  http://localhost:8100/xhr')
})
